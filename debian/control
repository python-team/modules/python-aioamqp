Source: python-aioamqp
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Michael Fladischer <fladi@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 python3-all,
 python3-pamqp (>= 3.1~),
 python3-setuptools,
 python3-sphinx,
 python3-sphinx-rtd-theme,
Standards-Version: 4.6.0.1
Homepage: https://github.com/Polyconseil/aioamqp/
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-aioamqp
Vcs-Git: https://salsa.debian.org/python-team/packages/python-aioamqp.git
Testsuite: autopkgtest-pkg-python
Rules-Requires-Root: no

Package: python-aioamqp-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Multi-Arch: foreign
Description: AMQP implementation using asyncio (Documentation)
 aioamqp library is a pure-Python implementation of the AMQP 0.9.1 protocol.
 .
 Built on top on Python's asynchronous I/O support introduced in PEP 3156, it
 provides an API based on coroutines, making it easy to write highly concurrent
 applications.
 .
 This package contains the documentation.

Package: python3-aioamqp
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 python-aioamqp-doc,
Description: AMQP implementation using asyncio (Python3 version)
 aioamqp library is a pure-Python implementation of the AMQP 0.9.1 protocol.
 .
 Built on top on Python's asynchronous I/O support introduced in PEP 3156, it
 provides an API based on coroutines, making it easy to write highly concurrent
 applications.
 .
 This package contains the Python 3 version of the library.
